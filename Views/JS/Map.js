'use strict';

//Map class
class Map {
    constructor(initObject) {

        console.log("Constructing Map object");

        this.mapDiv = initObject.mapDiv;

        this.mapData = [];
        this.trainMarkers = [];
        this.mymap = 0;

        this.InitializeMap();
    }

    //Initialize map
    InitializeMap() {

        this.mymap = L.map(this.mapDiv).setView([65.312, 27.466], 5);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoidGhlZnVycm93IiwiYSI6ImNqczBuNXFmdDFmeHg0M281Y3lqaG04M3IifQ.6RGVSnzIvR6wnJWLvqPsDQ'
        }).addTo(this.mymap);
    }

    //Parse data and send update request to the Markers
    UpdateLocations(data) {

        //First time update, initialize and create markers
        if (this.mapData[0] == undefined && data.length > 0) {
            this.mapData = data[0];

            let long, lat, train, type;

            for (var i = 0; i < data[0].length; i++) {
               
                long = (data[0][i].location.coordinates[0]) ? data[0][i].location.coordinates[0] : 0;
                lat = (data[0][i].location.coordinates[1]) ? data[0][i].location.coordinates[1] : 0;
                train = (data[0][i].trainNumber) ? data[0][i].trainNumber : undefined;
                type = (data[0][i].trainData) ? data[0][i].trainData.trainType : undefined;

                //Create marker and save to the array
                if (type != undefined){
                    this.trainMarkers.push(new Marker({
                        long: long,
                        lat: lat,
                        train: train,
                        type: type
                    }, this.mymap));
                }

            }
  
        }
        else {

            this.mapData = data[0];

            //Loop each marker in map and pass update data
            $.each(this.trainMarkers, function (index, row) {
                if (data[0][index] != undefined) {
                    if (row.trainNumber == data[0][index].trainNumber) {
                     
                        row.UpdateLocation([data[0][index].location.coordinates[1], data[0][index].location.coordinates[0]], data[0][index].trainData);
                    }
                    
                }
               
            })
        }
    }
}
