'use strict';

//Make class visible in window-environment (browser)
window.Marker = class Marker {
    constructor(initObject, map) {

        console.log("Constructing Marker object for map");

        this.trainNumber = initObject.train;
        this.trainType = initObject.type;
        this.mymap = map;
        this.marker = 0;
        this.popup = 0;
        this.icon = 0;
        this.latCoord = initObject.lat;
        this.longCoord = initObject.long;

        
        this.Initialize();
    }

    //Initialize marker 
    Initialize() {

        //Create icon in the map
        this.icon = new L.divIcon({
            className: 'marker-train',
            html: '<img class="marker-train" src="/Style/train-icon.png" />'+
                  '<h3 class="header-train-marker">' +this.trainType + this.trainNumber +'</h3>'
        });

        //Create popup for the marker
        this.popup = new L.popup().setLatLng([this.latCoord, this.longCoord]).setContent('<h4>' + this.GetTrainNameByType(this.trainType) + '</h4><p>' + this.trainType + this.trainNumber +'</p>');
        
        //Create marker
        this.marker = new L.marker([this.latCoord, this.longCoord], { icon: this.icon }).bindPopup(this.popup).addTo(this.mymap)
    }

    //Update location, called from Map-object
    UpdateLocation(newLocation, data) {
        let date = new Date().toJSON().replace(/-/, '-');
        let html = '';
        let popup = '';
        if (data.timeTableRows) {
            for (var index in data.timeTableRows) {
                if (data.timeTableRows[index].liveEstimateTime != undefined && data.timeTableRows[index - 1] != undefined) {
                    if (data.timeTableRows[index].liveEstimateTime > date &&
                   data.timeTableRows[index - 1].liveEstimateTime < date && data.timeTableRows[index].differenceInMinutes > 5) {


                        html = '<img class="marker-train" src="/Style/train-late-icon.png" />' +
                              '<h3 class="header-train-marker">' + this.trainType + this.trainNumber + '</h3>';
                        popup = '<h4>' + this.GetTrainNameByType(this.trainType) + '</h4><p>' + this.trainType + this.trainNumber + '</p><p>My&ouml;h&auml;ss&auml;: ' + data.timeTableRows[index].differenceInMinutes + 'min';

                        break;
                    }
                    else {
                        html = '<img class="marker-train" src="/Style/train-icon.png" />' +
              '<h3 class="header-train-marker">' + this.trainType + this.trainNumber + '</h3>';
                        popup = '<h4>' + this.GetTrainNameByType(this.trainType) + '</h4><p>' + this.trainType + this.trainNumber + '</p>';
                    }
                }

            }

        }
        
        //Update popup and icon
        this.popup.setContent(popup);
        this.icon.options.html = html;

        //Set new icon to marker
        this.marker.setIcon(this.icon);


        //Update location
        this.marker.setLatLng(L.latLng(newLocation)).bindPopup(this.popup);
    }

    //Return train name by its type
    GetTrainNameByType(type) {
        switch (type) {
            case "IC": { return "InterCity"; break;}
            case "IC2": { return "InterCity"; break; }
            case "AE": { return "Allegro"; break; }
            case "PYO": { return "Pendolino, y&ouml"; break; }
            case "H": { return "Taajamajuna"; break; }
            case "S": { return "Pendolino"; break; }
            default: { return type; break;}
        }
    }
}