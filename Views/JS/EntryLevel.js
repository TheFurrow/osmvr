//Start functionalities when document is ready
$(document).ready(function () {

    //Construct map object
    var map = new Map({
        mapDiv: 'mapid'
    });

    //Socket
    const socket = io();

    //Receive data from the server
    socket.on('trainData', function (data) {
        if (map != undefined) {
            //Pass data to Map object
            map.UpdateLocations(data);
        }
    })
});