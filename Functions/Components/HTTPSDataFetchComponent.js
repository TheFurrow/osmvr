'use strict';

//Require ComponentBase class
var ComponentBase = require('./ComponentBase.js').ComponentBase;
var HTTPSDataFetchInterface = require('../Interfaces/HTTPSDataFetchInterface.js').HTTPSDataFetchInterface;
var https = require('https');

class HTTPSDataFetchComponent extends ComponentBase {

    constructor(constructObject, parentId) {
        super(constructObject, parentId);

        this.callUrl = constructObject.constructObject[0].callUrl;
        
        this.initParams = constructObject.constructObject[0].initParams[0].callUrls;

        this.bIsFetching = false;

        //Construction objects for the interfaces
        this.constructInterfaces = [
            {
                callUrl: this.initParams[0].url,
                type: this.initParams[0].type,
                nameOfInterface: 'HTTPSDataFetchInterface0'
            },
            {
                callUrl: this.initParams[1].url,
                type: this.initParams[1].type,
                nameOfInterface: 'HTTPSDataFetchInterface1'
            },
            {
                callUrl: this.initParams[2].url,
                type: this.initParams[2].type,
                nameOfInterface: 'HTTPSDataFetchInterface2'
            }
        ];

        //Construct interfaces and push the into the array
        for(var index in this.constructInterfaces) {
            this.httpsDataInterface.push(new HTTPSDataFetchInterface(this.constructInterfaces[index], this.parentActor));
        }

        this.Update();
    }

    //Fetches data when parent requests
    FetchData() {
        for(var index in this.httpsDataInterface) {
            this.httpsDataInterface[index].FetchData();
        }
    }

    //Inherited from the base class
    Update() {
        this.FetchData();
    }
}

//Export this class outside of this location
module.exports.HTTPSDataFetchComponent = HTTPSDataFetchComponent;