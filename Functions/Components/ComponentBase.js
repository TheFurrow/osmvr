'use strict';

//Base class for components
class ComponentBase {

    //Constructor
    constructor(constructObject, parentActor) {
        
        this.classId = Math.random(0, 1000);
        this.componentName = constructObject.nameOfComponent;

        console.log("Constructing " + this.componentName);

        this.parentActor = parentActor;

        //Array holding interfaces constructed for this component
        this.httpsDataInterface = [];

        this.Initialize();
    }

    //Initialize function base
    Initialize() {
        
    }

    //GetCallUrl function base
    GetCallUrl() {
        return;
    }

    //GetParentActor function base
    GetParentActor() {
        return this.parentActor;
    }

    //Required
    //Update function base
    Update() {
        return;
    }

}

module.exports.ComponentBase = ComponentBase;