'use strict';

var ComponentBase = require('./ComponentBase.js').ComponentBase;
var app = require('../../server.js').app;
var http = require('http').Server(app);

//Required for emitting
var io = require('../../server.js').io;


class SocketIOComponent extends ComponentBase {

    //Constructor
    constructor(constructObject, parentId) {

        super(constructObject, parentId);

        this.emitUrl = constructObject.constructObject.emitUrl;

    }

    //Emit the data when requested
    Emit() {
        //Emits data to connected clients
        io.emit('trainData', this.parentActor.GetData());
    }

    //Returns count of connected clients
    GetConnectedClientAmount() {
        return io.engine.clientsCount;
    }

    //Required
    Update() {
        this.Emit();
    }
}



module.exports.SocketIOComponent = SocketIOComponent;