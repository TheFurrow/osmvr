'use strict';

//Require Actor base class
var Actor = require('./Actor.js').Actor;


class DataFetcher extends Actor {
    
    constructor(constructObject) {
        super(constructObject);

        this.dataNew = [];
        this.dataPrevious = [];

        this.dataTrainTypes = [];
        this.dataTrains = [];

        this.dataFirstFetch = true;

        //First time update
        for (var i = 0; i < this.componentList.length; i++) {
            this.componentList[i].Update();
        }
    }

    //Update each component with given interval
    //Inherited from Actor base class
    Tick() {
        super.Tick();

        //Should we update anything or do we just idle
        if (this.GetComponentByName("SocketIOComponent").GetConnectedClientAmount() > 0) {

            this.dataFirstFetch = false;

            for (var i = 0; i < this.componentList.length; i++) {
                this.componentList[i].Update();
            }
        }
    }

    RequestSocketData() {
        var socketIOComp = this.GetComponentByName("SocketIOComponent");
        if (socketIOComp != undefined) {
            socketIOComp.Update();
        }
    }

    //Return parsed data
    //Inherited from Actor base class
    GetData() {
        var dataTrains = this.dataTrains;
        var collection = [];
        collection.push(this.dataNew);

        for (var row in collection[0]) {
            for (var train in dataTrains) {

                if (collection[0][row].trainNumber == dataTrains[train].trainNumber) {
                    collection[0][row].trainData = dataTrains[train];
                }
            }
        }

        return collection;
    }

    //Receive, parse and collect
    ReceiveData(data, type) {
        switch (type) {
            case 'trainType': {
                this.dataTrainTypes = [];
                //Parse categories
                for (var i = 0; i < data.length; i++) {
                    if (data[i].trainCategory.name == 'Cargo' ||
                        data[i].trainCategory.name == 'Shunting' ||
                        data[i].trainCategory.name == 'Test drive' ||
                        data[i].trainCategory.name == 'On-track machines' ||
                        data[i].trainCategory.name == 'Locomotive') {
                        data.splice(i, 1);
                    }
                    else {
                        this.dataTrainTypes.push(data[i]);
                    }

                }

                break;
            }
            case 'trains': {
                this.dataTrains = [];
                //Parse categories
                for (var i = 0; i < data.length; i++) {
                    if (data[i].trainCategory == 'Cargo' ||
                        data[i].trainCategory == 'Shunting' ||
                        data[i].trainCategory == 'Test drive' ||
                        data[i].trainCategory == 'On-track machines' ||
                        data[i].trainCategory == 'Locomotive') {
                        data.splice(i, 1);
                    }
                    else {
                        this.dataTrains.push(data[i]);
                    }

                }

                break;
            }

            case 'trainLocations': {
                //Set previous data for later use (.ie calculate direction for animation)
                this.dataPrevious = this.dataNew;

                //Set new data
                this.dataNew = data;

                break;
            }
        }
    }

}

//Export so this class can be used anywhere else
module.exports.DataFetcher = DataFetcher;