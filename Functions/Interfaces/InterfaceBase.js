'use strict';

//Base class for interfaces
class InterfaceBase {

    //Constructor
    constructor(constructObject, parentActor) {
        
        this.classId = Math.random(0, 1000);
        this.interfaceName = constructObject.nameOfInterface;

        console.log("Constructing " + this.interfaceName);

        this.parentActor = parentActor;
    }

    FetchData() {

    }

    //GetCallUrl function base
    GetCallUrl() {
        return;
    }

    //GetParentActor function base
    GetParentActor() {
        return this.parentActor;
    }

}

module.exports.InterfaceBase = InterfaceBase;