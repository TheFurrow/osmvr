'use strict';

//Require ComponentBase class
var InterfaceBase = require('./InterfaceBase.js').InterfaceBase;
var https = require('https');


class HTTPSDataFetchInterface extends InterfaceBase {

    constructor(constructObject, parentId) {
        super(constructObject, parentId);

        this.callUrl = constructObject.callUrl;
        this.type = constructObject.type;

    }

    //Fetches data when parent requests
    FetchData() {

        https.get(this.callUrl, (response) => {
            var buffer = '';
            response.on('data', (bufferBlock) => {
                buffer += bufferBlock
            });

            response.on('end', () => {
                this.parentActor.ReceiveData(JSON.parse(buffer), this.type);
            });
        }).on('error', (err) => { return false; });
    }

}

//Export this class outside of this location
module.exports.HTTPSDataFetchInterface = HTTPSDataFetchInterface;