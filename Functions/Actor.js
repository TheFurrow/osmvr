'use strict';

var HTTPSDataFetchComponent = require('./Components/HTTPSDataFetchComponent.js');
var SocketIOComponent = require('./Components/SocketIOComponent.js');


//Base class for actors
class Actor {

    //Constructor
    constructor(constructObject) {

        console.log('Constructing actor');

        this.self = this;

        this.classId = Math.random(0, 1000);

        //How often we update our data in if real time is disabled?
        this.normalUpdateInterval = constructObject.normalUpdateInterval;

        //How often we update our data in real time?
        this.realTimeUpdateInterval = constructObject.realTimeUpdateInterval;

        //Should we use normal interval or realtime?
        this.bUseRealTimeInterval = constructObject.bUseRealTimeInterval;

        //Does this actor tick at all?
        this.bCanTick = constructObject.bCanTick;

        //Components that are helping this actor
        this.componentList = [];

        //Construct components and push them in the list
        function ConstructComponents(constructObject, parentId, componentList) {
            console.log('Actor begins constructing components...');
            var list = [];
 
            for (var i = 0; i < constructObject.components.length; i++) {
                switch (constructObject.components[i].nameOfComponent) {
                    case 'HTTPSDataFetchComponent': {
                        componentList.push(new HTTPSDataFetchComponent.HTTPSDataFetchComponent(constructObject.components[i], parentId));
                        
                        break;
                    }
                    case 'SocketIOComponent': {
                        componentList.push(new SocketIOComponent.SocketIOComponent(constructObject.components[i], parentId));

                        break;
                    }
                }
            }

        } ConstructComponents(constructObject, this.self, this.componentList);

        this.data = [];

        this.updateInterval = (this.bUseRealTimeInterval) ? this.realTimeUpdateInterval : this.normalUpdateInterval;

        //Should this actor  tick or not?
        if (this.bCanTick) {
            var actor = this;
            setInterval(function () { actor.Tick() }, this.updateInterval);

            //Make first Tick call
            this.Tick();
        }
    }

    //Tick function base
    Tick() {

    }

    //Return component from the array by its name
    GetComponentByName(compName) {
        for (var index in this.componentList) {
            if (this.componentList[index].componentName == compName) {
                return this.componentList[index];
                break;
            }
           
        }
    }

    //GetData function base
    GetData() {
        return data;
    }

    //GetId function base
    GetId() {
        return this.classId;
    }

}

//Export so this class can be used anywhere else
module.exports.Actor = Actor;