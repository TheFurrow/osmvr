var express = require('express');

//Export Express for Components
var app = express();
module.exports.app = app;

var http = require('http').Server(app);
var cookieparser = require('cookie-parser');
var expressSession = require('express-session');
var bodyParser = require('body-parser');

//Export Socket.IO for Components
const io = require('socket.io')(http);
module.exports.io = io;

var serverConfig = require('./Configuration/ServerConfiguration.js');

//Classes
var DataFetcher = require('./Functions/DataFetcher.js');

//Actors
var Actors = [];


///Functions

//Make actors
function MakeActors() {

    //Create object array of objects that are later on passed for Actor constructors
    var constructObjects = {
        classes: [
            {
                type: 'TrainData',
                bCanTick: true,
                bUseRealTimeInterval: false,
                normalUpdateInterval: 5000,
                realTimeUpdateInterval: 200,
                components: [
                    {
                        nameOfComponent: 'HTTPSDataFetchComponent',
                        constructObject: [{
                            parentActorId: null,
                            callUrl: 'https://rata.digitraffic.fi/api/v1/train-locations/latest/',
                            initParams: [
                                {
                                    callUrls: [
                                         {
                                             url: 'https://rata.digitraffic.fi/api/v1/train-locations/latest/',
                                             type: 'trainLocations'
                                         },
                                        {
                                            url: 'https://rata.digitraffic.fi/api/v1/metadata/train-types',
                                            type: 'trainTypes'
                                        },
                                        {
                                            url: 'https://rata.digitraffic.fi/api/v1/live-trains/',
                                            type: 'trains'
                                        }
                                    ]
                                }
                            ]
                        }]
                    },
                    {
                        nameOfComponent: 'SocketIOComponent',
                        constructObject: {
                            parentActorId: null,
                            emitUrl: 'trainData'
                        }
                    }
                ]
            }
        ]
    }

    ConstructActors(constructObjects);
}

//Construct actors
function ConstructActors(constructObjects) {
    for (var i = 0; i < constructObjects.classes.length; i++) {
        switch (constructObjects.classes[i].type) {
            case 'TrainData': {
                var DFetcher = new DataFetcher.DataFetcher(constructObjects.classes[i]);
                Actors.push(DFetcher);
                break;
            }
        }
    }
}

///App

app.use(cookieparser());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '5mb',
    parameterLimit: 10000000000
}));
app.use(expressSession(serverConfig.session));
app.disable('view cache');


app.set('views', __dirname + '/Views');
app.set('view engine', 'ejs');

app.use("/", express.static('Views'));

///Socket

//For fist time connection
io.on('connection', function (socket) {
    if (Actors.length > 0) {
        for (var index in Actors) {
            if(Actors[index].constructor.name == "DataFetcher") {
                Actors[index].RequestSocketData();
            }
            
        }
    }
})


//Index
app.get('/', function (req, res) {

    res.render('index');
});


//404
app.get('*', function (req, res) {
    res.render('404');
});

var port = process.env.PORT || 80;

http.listen(port, function () {

    //Create our objects when server is running
    MakeActors();

    console.log('Running server on ' + port);
});
